var i = 0;

function move() 
{
	if (i == 0) 
	{
		i = 1;
		var elem = document.getElementById("myBar");
		var width = 1;
		var id = setInterval(frame, 10);
		function frame() 
		{
			if (width >= 100) 
			{
				clearInterval(id);
				i = 0;
			} 
			else 
			{
				width++;
				elem.style.width = width + "%";
			}
		}
	}
}

function help() 
{
	//window.alert("Choose a file to update, then press upload. A load bar will appear showing the progress of the upload.\nIf a problem occurs the files can be roled back to an older version by selecting one of the files in archive and pressing restore.");
}

function myFunction() 
{
  document.getElementById("demo").style.fontSize = "25px"; 
  document.getElementById("demo").style.color = "red";
  document.getElementById("demo").style.backgroundColor = "yellow";        
}

function setpath() 
{
	var default_path = document.getElementById("newfile").files[0].name;
	document.getElementById("filepath").value = default_path;
}

function upload() 
{
	var filePath = document.getElementById("filepath").value;
	var upload_path = "/upload/" + filePath;
	var fileInput = document.getElementById("newfile").files;

	/* Max size of an individual file. Make sure this
	 * value is same as that set in file_server.c */
	var MAX_FILE_SIZE = 200*1024;
	var MAX_FILE_SIZE_STR = "200KB";

	if (fileInput.length == 0) 
	{
		alert("No file selected!");
	} 
	else if (filePath.length == 0) 
	{
		alert("File path on server is not set!");
	} 
	else if (filePath.indexOf(' ') >= 0) 
	{
		alert("File path on server cannot have spaces!");
	} 
	else if (filePath[filePath.length-1] == '/') 
	{
		alert("File name not specified after path!");
	} 
	else if (fileInput[0].size > 200*1024) 
	{
		alert("File size must be less than 200KB!");
	} 
	else 
	{
		document.getElementById("newfile").disabled = true;
		document.getElementById("filepath").disabled = true;
		document.getElementById("upload").disabled = true;

		var file = fileInput[0];
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() 
		{
			if (xhttp.readyState == 4) 
			{
				if (xhttp.status == 200) 
				{
					document.open();
					document.write(xhttp.responseText);
					document.close();
				} 
				else if (xhttp.status == 0) 
				{
					alert("Server closed the connection abruptly!");
					location.reload()
				} 
				else 
				{
					alert(xhttp.status + " Error!\n" + xhttp.responseText);
					location.reload()
				}
			}
		};
		xhttp.open("POST", upload_path, true);
		xhttp.send(file);
	}
}

function change()
{
	var selectedValue = document.querySelector("#progress-value").value;
	document.querySelector(".progress-bar-striped > div").textContent = selectedValue + "%";
	document.querySelector(".progress-bar-striped > div").style.width = selectedValue + "%";
}